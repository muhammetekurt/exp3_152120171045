/*
 * Circle.h
 *
 *  Created on: 8 Kas 2018
 *      Author: ykartal
 */

#ifndef CIRCLE_H_
#define CIRCLE_H_
class Circle {
public:
	Circle(double);
	virtual ~Circle();
	void setR(double);
	void setR(int);
	double getR()const;
	double calculateCircumference()const;
	double calculateArea();
	bool compare(double, double);
private:
	double r;
	const double PI = 3.14;
};
#endif /* CIRCLE_H_ */
